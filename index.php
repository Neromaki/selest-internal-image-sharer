<!DOCTYPE html>
<html lang="en">
    <head>
        <title>selesti internal imgur</title>
        <link rel="stylesheet" type="text/css" href="assets/css/main.css">
    </head>

    <body>
        <img src="assets/img/selesti-logo.png" id="logo" />
        <form method="post" action="upload.php" enctype="multipart/form-data">
            <div class="image-preview-container">
                <img id="uploadPreview" onclick="chooseFile()" src="assets/img/camera_icon_dark.png" />
            </div>
            <input id="uploadImage" type="file" name="image_file" onchange="PreviewImage();" />
            <div class="upload-button-container">
                <button type="submit" name="submit" id="uploadButton">Upload</button>
            </div>
        </form>

        <script type="text/javascript" src="assets/js/main.js"></script>
    </body>
</html>